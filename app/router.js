const productRouter = require ('./Components/Product/router')
const categoryRouter= require('./Components/Category/router')

module.exports = (app) =>{
	app.use('/api/v1/product',productRouter)
	app.use('/api/v1/category',categoryRouter)
}

