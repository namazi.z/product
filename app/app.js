const express= require('express')
require('../db/Connections/mongo')

const app = express()

require('./Middleware')(app)
require('./router')(app)

const startApp= ()=>{
	app.listen(process.env.APP_PORT ,()=>{
		console.log(`Server is running on port ${process.env.APP_PORT}`)
	})
}

module.exports= startApp