const categoryService = require('./categoryService')

exports.saveInfo = async (req, res) => {
	let newCat = {...req.body};
	const newCategory= await categoryService.create(newCat)
	if (newCategory) {
        return res.send({
            success: true,
            message: 'ثبت دسته بندی با موفقیت انجام شد'
        });
    }
    return res.send({
        success: false,
        message: 'خطایی در فرآیند ثبت رخ داده است'
    });
}

