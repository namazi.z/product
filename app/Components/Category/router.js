const express = require('express')
const categoryRouter = express.Router()
const categoryHandler = require('./handler')

categoryRouter.post('/',categoryHandler.saveInfo)

module.exports =categoryRouter