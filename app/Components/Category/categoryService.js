const Category = require('../../../db/models/mongo/category')

exports.create = async (params) => {

	const category = new Category({
		...params
	})
    return await category.save()
}

