const productService = require('./productService')

exports.saveInfo = async (req, res) => {

    try {
        let product = { ...req.body };

        const newProduct = await productService.create(product)
        if (newProduct) {
            return res.send({
                success: true,
                message: 'ثبت محصول با موفقیت انجام شد'
            });
        }
        return res.send({
            success: false,
            message: 'خطایی در فرآیند ثبت رخ داده است'
        });


    } catch (error) {
        res.status(500).send()
    }

}

exports.getInfo = async (req, res) => {
    try {
        const products = await productService.get()
        return res.send(products)
    } catch (error) {

    }
}

exports.update = async (req, res) => {
	try {
		const updates = Object.keys(req.body)
		const allowedUpdate = ['title', 'slug','price','description']
		const isValidOpertion = updates.every(update => allowedUpdate.includes(update))

		if (!isValidOpertion) {
            return	res.status(400).send({ error: " Invalid Updates!" })
		}

		const product = await productService.findAndUpdate(req.params.id, req.body)
		if (!product) {
			return res.status(404).send()
		}
		return res.send(product)

	} catch (error) {
		res.status(400).send(error)
	}
}

exports.delete = async (req, res) => {
	try {
		const product = await productService.findAndDelete({ _id: req.params.id })
		if (!product) {
			return res.status(404).send()
		}
		 return res.send(product)

	} catch (error) {
		res.status(500).send()
	}
}

exports.search =async (req,res)=>{
    try {
        let keyword = req.query.keyword
          const resultSearch = await productService.search(keyword)
        if(resultSearch){
           return res.send(resultSearch)
        }
    } catch (error) {
        
    }
}
