const express = require('express')
const productRouter = express.Router()
const productHandler = require('./handler')

productRouter.get('/', productHandler.getInfo)
productRouter.post('/', productHandler.saveInfo)
productRouter.patch('/:id', productHandler.update)
productRouter.delete('/:id', productHandler.delete)

productRouter.get('/search', productHandler.search)

module.exports = productRouter

