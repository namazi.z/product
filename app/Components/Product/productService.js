const Product = require('../../../db/models/mongo/product')

exports.create = async (params) => {
	const product = new Product({
		...params
	})
	const newProduct = await product.save()
	return newProduct
}

exports.get = async () => {
	let products = await Product.find({}).populate('category').exec()

	return products
}

exports.findAndUpdate = async (id, params) => {
	const product = await Product.findByIdAndUpdate(id, params, { new: true, runValidators: true })
	return product
}

exports.findAndDelete = async (id) => {
	const product = await Product.findByIdAndDelete(id)
	return product
}

exports.search = async (params) => {
	let result = await Product.find(
		{
			$or: [{ title: { $regex: '.*' + params + '.*' } },
			{ description: { $regex: '.*' + params + '.*' } }]
		}
	)
	return result
}