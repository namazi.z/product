const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	title:{
		type: String,
		required: true,
		trim: true
	},
	slug: String,
	price: Number,
	sale: Number,
	stock: Boolean,
	description: String,
	category: {
		type: mongoose.Schema.Types.ObjectId,
		require: true,
		ref: 'Category'
	},
	attributes: mongoose.Schema.Types.Mixed

});

module.exports = mongoose.model('Product', productSchema);
