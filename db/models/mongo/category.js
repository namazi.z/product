const mongoose= require('mongoose')
const categorySchema = new mongoose.Schema({
	fa_title:{
		type: String,
		required: true,
		trim: true
	},
	en_title:{
		type: String,
		required: true,
		trim: true
	},
	attributes: mongoose.Schema.Types.Mixed
})

module.exports = mongoose.model('Category', categorySchema);
