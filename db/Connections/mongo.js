const mongoos =require('mongoose');

mongoos.connect(`${process.env.MONGO_URL}/${process.env.MONGO_DB}`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(()=>{
	console.log("Mongo db Connected")
}).catch(err =>{
	console.log("Connection to MONGO failed",err)
});